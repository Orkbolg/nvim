local wk = require("which-key")

local function cut_and_insert_after()
    -- Cut the selected text
    vim.cmd('normal! "vd')

    -- Enter insert mode
    vim.cmd('startinsert')

    -- Create an autocommand to insert the cut text after leaving insert mode
    vim.api.nvim_create_autocmd("InsertLeave", {
        once = true,  -- This ensures the command only runs once
        callback = function()
            -- Insert the text from the unnamed register
            vim.cmd('normal! "vP')
        end,
    })
end

-- Expose the function globally so it can be used in a keymap
_G.cut_and_insert_after = cut_and_insert_after

-- Map this function to a keybinding, e.g., <leader>c
vim.api.nvim_set_keymap('v', '<leader>c', '<cmd>lua cut_and_insert_after()<CR>', { noremap = true, silent = true })

wk.add({
    { "<leader>c",  desc = "surround",    mode = "v" },
})
