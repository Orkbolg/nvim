local wk = require("which-key")

require("zen-mode").setup({
    window = {
        backdrop = 0.95,
        height = 1,
        options = {
            signcolumn = "no",
            number = false,
            relativenumber = false,
        }
    },
    plugins = {
        tmux = { enabled = false },
    }
})

vim.keymap.set("n", "<leader>ez", vim.cmd.ZenMode)

wk.add({
    {"<leader>e", group = "editor"},
    {"<leader>ez", desc =  "zenmode" }
})
