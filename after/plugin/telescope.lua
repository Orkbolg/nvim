local wk = require("which-key")
local builtin = require('telescope.builtin')

vim.keymap.set('n', '<leader>pf', builtin.find_files, {})
vim.keymap.set('n', '<leader>pg', builtin.git_files, {})
vim.keymap.set('n', '<leader>ps', function()
    builtin.grep_string({ search = vim.fn.input("Grep > ") });
end)

wk.add({
    {"<leader>p", group = "telescope" },
    {"<leader>pf", desc = "files" },
    {"<leader>pg", desc = "git files" },
    {"<leader>ps", desc = "grep" },
    {"<leader>pv", desc = "file explorer" },
})

