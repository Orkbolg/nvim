local wk = require("which-key")

local edit_leader = "<leader>m"

vim.keymap.set("v", edit_leader.."t", ":!column -t -s '|' -o '|'")

wk.add({
    { edit_leader,  group = "markdown",    mode = "v" },
    { edit_leader.."t", desc = "format table", mode = "v" },
})

