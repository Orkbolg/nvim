local lsp_zero = require('lsp-zero')
local wk = require("which-key")

lsp_zero.on_attach(function(client, bufnr)
    -- see :help lsp-zero-keybindings
    -- to learn the available actions
    lsp_zero.default_keymaps({ buffer = bufnr })
end)

require('mason').setup({})
require('mason-lspconfig').setup({
    ensure_installed = {
        'eslint',
        'lua_ls',
        'rust_analyzer',
        'clangd',
        'bashls',
        'angularls',
        'cssls',
    },
    handlers = {
        lsp_zero.default_setup,
    },
})

lsp_zero.set_preferences({
    sign_icons = {}
})

local cmp = require('cmp')

cmp.setup({
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
        ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
        ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
        ['<C-y>'] = cmp.mapping.confirm({ select = true }),
        ['<C-Space>'] = cmp.mapping.complete(),
    })
})

lsp_zero.on_attach(function(client, bufnr)
    local opts = { buffer = bufnr, remap = false }

    vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
    vim.keymap.set("n", "<leader>vs", function() vim.lsp.buf.workspace_symbol() end, opts)
    vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, opts)
    vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
    vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
    vim.keymap.set("n", "<leader>va", function() vim.lsp.buf.code_action() end, opts)
    vim.keymap.set("n", "<leader>vr", function() vim.lsp.buf.references() end, opts)
    vim.keymap.set("n", "<leader>vn", function() vim.lsp.buf.rename() end, opts)
    vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
    vim.keymap.set("n", "<leader>vi", function() vim.lsp.buf.implementation() end, opts)
end)

lsp_zero.setup()

wk.add({
    { "gd",          desc = "lsp go definition" },
    { "K",           desc = "lsp hover" },
    { "<leader>v",   group = "lsp" },
    { "<leader>vw", desc = "lsp workplace symbol" },
    { "<leader>vd",  desc = "lsp open diagnostic" },
    { "[d",          desc = "lsp diagnostic goto next" },
    { "d{",          desc = "lsp diagnostic goto previous" },
    { "<leader>va", desc = "lsp code action" },
    { "<leader>vr", desc = "lsp references" },
    { "<leader>vn", desc = "lsp rename" },
    { "<leader>vi", desc = "lsp implementation" },
})
