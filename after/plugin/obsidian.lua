local wk = require("which-key")

local obsidian_vault = os.getenv("OBSIDIAN_VAULT")
local obsidian_leader = "<leader>o"

vim.keymap.set("n", obsidian_leader.."o", ":cd "..obsidian_vault.."<cr><cr>")
vim.keymap.set("n", obsidian_leader.."n", ":ObsidianNew<cr>")
vim.keymap.set("n", obsidian_leader.."a", ":ObsidianTags<cr>")
vim.keymap.set("n", obsidian_leader.."f", ":Telescope find_files search_dirs="..obsidian_vault.."<cr>")

vim.keymap.set("n", obsidian_leader.."z", ":Telescope live_grep search_dirs="..obsidian_vault.."<cr>")
vim.keymap.set("n", obsidian_leader.."t", ":ObsidianToday<cr>")
vim.keymap.set("n", obsidian_leader.."y", ":ObsidianYesterday<cr>")
vim.keymap.set("n", obsidian_leader.."b", ":ObsidianBacklinks<cr>")
vim.keymap.set("n", obsidian_leader.."l", ":ObsidianLinks<cr>")
vim.keymap.set("n", obsidian_leader.."d", ":ObsidianDailies<cr>")
vim.keymap.set("v", obsidian_leader.."e", ":ObsidianExtractNote<cr>")

wk.add({
    { obsidian_leader,  group = "obsidian" },
    { obsidian_leader.."o", desc = "goto vault" },
    { obsidian_leader.."n", desc = "new note" },
    { obsidian_leader.."f", desc = "find note" },
    { obsidian_leader.."z", desc = "grep notes" },
    { obsidian_leader.."a", desc = "search tags" },
    { obsidian_leader.."t", desc = "note today" },
    { obsidian_leader.."y", desc = "note yesterday" },
    { obsidian_leader.."b", desc = "backlinks" },
    { obsidian_leader.."l", desc = "links" },
    { obsidian_leader.."d", desc = "dailies" },
})

wk.add({
    { obsidian_leader,  group = "obsidian",    mode = "v" },
    { obsidian_leader.."e", desc = "extract note", mode = "v" },
})
