local mark = require("harpoon.mark")
local ui = require("harpoon.ui")
local wk = require("which-key")

vim.keymap.set("n", "<leader>ha", mark.add_file)
vim.keymap.set("n", "<leader>he", ui.toggle_quick_menu)

vim.keymap.set("n", "<leader>hh", function() ui.nav_file(1) end)
vim.keymap.set("n", "<leader>ht", function() ui.nav_file(2) end)
vim.keymap.set("n", "<leader>hn", function() ui.nav_file(3) end)
vim.keymap.set("n", "<leader>hs", function() ui.nav_file(4) end)

wk.add({
    {"<leader>h", group = "harpoon" },
    {"<leader>ha", desc = "add file" },
    {"<leader>he", desc = "quick menu" },
    {"<leader>hh", desc = "harpoon first file" },
    {"<leader>ht", desc = "harpoon second file" },
    {"<leader>hn", desc = "harpoon third file" },
    {"<leader>hs", desc = "harpoon fourth file" },
})

