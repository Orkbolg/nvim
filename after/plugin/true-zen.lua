local wk = require("which-key")

local editor_leader = "<leader>e"


vim.keymap.set("n", editor_leader.."f", ":TZNarrow<cr>")
vim.keymap.set("v", editor_leader.."f", ":TZNarrow<cr>")

wk.add({
    { editor_leader,  group = "editor", mode = "n" },
    { editor_leader.."f", desc = "focus", mode = "n" },
    { editor_leader,  group = "editor", mode = "v" },
    { editor_leader.."f", desc = "focus", mode = "v" },
})
