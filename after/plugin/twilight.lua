local wk = require("which-key")

vim.keymap.set("n", "<leader>et", vim.cmd.Twilight)

wk.add({
    {"<leader>et", desc = "twilight" }
})
