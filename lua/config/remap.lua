local wk = require("which-key")

vim.g.mapleader = " "

local keymap = vim.keymap
keymap.set("n", "<leader>pv", vim.cmd.Ex)

keymap.set("v", "J", ":m '>+1<CR>gv=gv")
keymap.set("v", "K", ":m '<-2<CR>gv=gv")

keymap.set("n", "Y", "yg$")
keymap.set("n", "J", "mzJ`z")
keymap.set("n", "<C-d>", "<C-d>zz")
keymap.set("n", "<C-u>", "<C-u>zz")
keymap.set("n", "n", "nzzzv")
keymap.set("n", "N", "Nzzzv")

keymap.set("n", "<leader>nh", ":noh<CR>", { desc = "Clear search highlights"})

keymap.set("n", "<leader>+", "<C-a>", { desc = "Increment number" })
keymap.set("n", "<leader>-", "<C-x>", { desc = "Decrement number" })

keymap.set("x", "<leader>p", "\"_dP")

keymap.set("n", "<leader>y", "\"+y")
keymap.set("v", "<leader>y", "\"+y")
keymap.set("n", "<leader>Y", "\"+Y")


keymap.set("n", "<leader>d", "\"_d")
keymap.set("v", "<leader>d", "\"_d")

keymap.set("i", "<C-c>", "<Esc>")

keymap.set("n", "Q", "<nop>")
keymap.set("n", "q:", ":q<CR>")

keymap.set("n", "<leader>f", function()
    vim.lsp.buf.format()
end)

local window_key = "<leader>w"
keymap.set("n", window_key .. "j", "<C-w>j")
keymap.set("n", window_key .. "k", "<C-w>k")
keymap.set("n", window_key .. "l", "<C-w>l")
keymap.set("n", window_key .. "h", "<C-w>h")
keymap.set("n", window_key .. "q", ":q<CR>")
keymap.set("n", window_key .. "v", "<C-w>v", { desc = "split vertical" })
keymap.set("n", window_key .. "s", "<C-w>s", { desc = "split horizontally" })
keymap.set("n", window_key .. "d", "<cmd>close<CR>", { desc = "close current split" })


local tab_key = "<leader>t"

keymap.set("n", tab_key .. "o", "<cmd>tabnew<CR>", { desc = "new tab" })
keymap.set("n", tab_key .. "d", "<cmd>tabclose<CR>", { desc = "close tab" })
keymap.set("n", tab_key .. "n", "<cmd>tabn<CR>", { desc = "next tab" })
keymap.set("n", tab_key .. "p", "<cmd>tabp<CR>", { desc = "previous tab" })
keymap.set("n", tab_key .. "f", "<cmd>tabnew %<CR>", { desc = "open current buffer in tab" })

--Buffer keymaps
local buffer_key = "<leader>b"
keymap.set("n", buffer_key .. "K", ":bufdo bd<CR>")
keymap.set("n", buffer_key .. "p", vim.cmd.bp)
keymap.set("n", buffer_key .. "n", vim.cmd.bn)
keymap.set("n", buffer_key .. "a", vim.cmd.ball)
keymap.set("n", "<tab>", vim.cmd.bn)
keymap.set("n", "<S-tab>", vim.cmd.bp)

wk.add({
    { "<leader>f",            function() vim.lsp.buf.format() end, desc = "format file" },
    {
        "<leader>b",
        group = "buffer",
        expand = function()
            return require("which-key.extras").expand.buf()
        end
    },
    { buffer_key .. "d",           vim.cmd.bdelete,                     desc = "delete" },
    { buffer_key .. "s",           vim.cmd.write,                       desc = "save" },
    { buffer_key .. "k",           vim.cmd.bdelete,                     desc = "delete" },
    { buffer_key .. "i",           ":Telescope buffers<CR>",            desc = "ibuffer" },
    { buffer_key .. "K",           ":bufdo bd<CR>",                     desc = "delete all" },
    { buffer_key .. "p",           vim.cmd.bp,                          desc = "previous" },
    { buffer_key .. "n",           vim.cmd.bn,                          desc = "next" },
    { buffer_key .. "a",           vim.cmd.ball,                        desc = "all" },
    { "<leader><tab>",        vim.cmd.bn,                          desc = "next buffer" },
    { "<leader><shift><tab>", vim.cmd.bp,                          desc = "next buffer" },
    { tab_key,              group = "tab" },
    { window_key,            group = "window" },
    { window_key .. "j",           desc = "switch down" },
    { window_key .. "k",           desc = "switch up" },
    { window_key .. "l",           desc = "switch right" },
    { window_key .. "h",           desc = "switch left" },
    { window_key .. "q",           desc = "quit" },
})

vim.cmd('command! Q q')
vim.cmd('command! W w')
