local wk = require("which-key")

local leader = "<space>"

function InsertDateStamp()
    local date = os.date("[%Y-%m-%d %a %H:%M]")
    vim.api.nvim_buf_set_lines(0, vim.fn.line('.') - 1, vim.fn.line('.') - 1, false, { date })
end

function InsertDate()
    local date = os.date("[%Y-%m-%d %a]")
    vim.api.nvim_buf_set_lines(0, vim.fn.line('.') - 1, vim.fn.line('.') - 1, false, { date })
end

vim.keymap.set("n", leader .. "es", ":lua InsertDateStamp()<CR>")
vim.keymap.set("n", leader .. "ed", ":lua InsertDate()<CR>")

wk.add({
    { leader .. "e", group = "editor" },
    { leader .. "es", desc = "insert date stamp" },
    { leader .. "ed", desc = "insert date" },
})


function CenterIfFewLines()
    local lines_in_view = vim.fn.line("w$") - vim.fn.line("w0") + 1
    if lines_in_view < 20 then
        vim.cmd("normal! zz")
    end
end

vim.api.nvim_create_autocmd('CursorMoved', {
    pattern = "*",
    callback = function() CenterIfFewLines() end
})

