return {
    "folke/twilight.nvim",
    opts = {
        treesitter = true,
        dimming = {
            alpha = 0.25,
        }
    }
}
