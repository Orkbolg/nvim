return {
    "Pocco81/true-zen.nvim",
    config = function()
        require("true-zen").setup {
            modes = {
                narrow = {
                    folds_style = "invisible",
                    run_ataraxis = false
                }
            }
        }
    end,
}
