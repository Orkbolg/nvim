return {
	'nvim-telescope/telescope.nvim',
    branch = "0.1.x",
	dependencies = {
        'nvim-lua/plenary.nvim',
        { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
        "nvim-tree/nvim-web-devicons",
    },
    config = function()
        local telescope = require('telescope')
        local actions = require('telescope.actions')

        telescope.setup {
            defaults = {
                path_display = { "smart" },
                mappings = {
                    i = {
                        ["<C-j>"] = actions.move_selection_next,
                        ["<C-k>"] = actions.move_selection_previous,
                        ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
                    },
                },
            }
        }

        local keymap = vim.keymap
        local project_key = "<leader>p"
        keymap.set("n", project_key .. "c", "<cmd>Telescope grep_string<cr>", { desc = "Find string under cursor" })
    end
}
